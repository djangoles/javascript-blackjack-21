// GLOBAL VARS
const rank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
// const rank = ['4', '5', '6', 'A'];
const suit = ['heart', 'diamond', 'spade', 'club'];

const gameState = {
    playerAces: false,
    playerAcesLength: [],
    playerHand: [],
    dealerHand: [],
    playerScore: 0,
    dealerScore: 0,
    playerWins: 0,
    dealerWins: 0
};

// DOM SELECTORS
const gameAreaBoard = document.querySelector('.game-area-board')
const headerTitleSpanContainer = document.querySelector('.header-title-span-container')
const headerTitleTopCenter = document.querySelector('.header__title-top-center');
const startBtn = document.querySelector('.start-btn');
const sideBar = document.querySelector('.sidebar');
const btnHit = document.querySelector('.btn-hit');
const btnHold = document.querySelector('.btn-hold');
const player1ResultsContainer = document.querySelector('.player-output-1');
const player2ResultsContainer = document.querySelector('.player-output-2');
const p1ScoreContainer = document.querySelector('.player1-results-score');
const p2ScoreContainer = document.querySelector('.player2-results-score');
let p1Score = document.querySelector('.player1-results-score span');
let p2Score = document.querySelector('.player2-results-score span');
const finalScore = document.querySelector('.final-score');
const playAgainModal = document.querySelector('.play-again-modal');
const playAgainBtn = document.querySelector('.play-again-btn');
const p1WinTotal = document.querySelector('.p1-win-total');
const p2WinTotal = document.querySelector('.p2-win-total');


playAgainBtn.addEventListener('click', ()=> {
    playAgainModal.classList.remove('play-again-modal-show');
    p1WinTotal.textContent = gameState.playerWins;
    p2WinTotal.textContent = gameState.dealerWins;
    resetState();
    resetUI();
    shuffleAndDeal();
})

//Create 52 Card deck with values for each card
const createDeck = (arr1, arr2) => {
    let fullDeck = []
    for (let i = 0; i < arr1.length; i++) {
       for (let j = 0; j < arr2.length; j++) {
            let tempObj = {}
            tempObj.suit = arr2[j]
            tempObj.rank = arr1[i]
            if(arr1[i] === 'J' || arr1[i] === 'Q' || arr1[i] === 'K') {
                tempObj.value = 10
            } else if(arr1[i] === 'A'){
                tempObj.value = 1
            } else {
                tempObj.value = parseInt(arr1[i])
            }
            fullDeck.push(tempObj)
       }    
    }
    return fullDeck
}

//Pass in createDeck() objArray return, create random index arr and return new deck from random indexes
const getShuffledDeck = (cardArrObj) => {
    let shuffledIndex = []
    let finalShuffled = []
    while(shuffledIndex.length < cardArrObj.length) {
        let randomIndex = Math.floor(Math.random() * cardArrObj.length)
        if(!shuffledIndex.includes(randomIndex)) {
            shuffledIndex.push(randomIndex)
        }   
    }
    for (let index = 0; index < shuffledIndex.length; index++) {
        finalShuffled.push(cardArrObj[shuffledIndex[index]])   
    }
    return finalShuffled
}

//Pass in single arr[] from getshuffled deck return
const createSingleCard = (card, i, arr) => {
    const suitColor =  getSuitColor(card)
    const suitSymbol = getSuitSymbol(card)
    const div = document.createElement('div')
    div.classList.add('card')
    const html = `
        <p class="card-rank ${suitColor}">${card.rank}</p>
        <p class="card-suit ${suitColor}">${suitSymbol}</p>
    `
    div.innerHTML = html
    return div
}

// Pass In single card to determine CSS class for color of card suit
const getSuitColor = (card) => {
    if(card.suit === 'spade' || card.suit === 'club' ) {
        return 'card-suit--black'
    } else {
        return 'card-suit--red'   
    }
} 

// Pass In single card to determine HTML ICON for suit
const getSuitSymbol = (card) => {
    let cardSymbol = ''
    if(card.suit === 'spade') {
        cardSymbol = '&spadesuit;'
    } else if(card.suit === 'club') {
        cardSymbol = '&clubsuit;'
    } else if(card.suit === 'diamond') {
        cardSymbol = '&diamondsuit;'  
    } else {
        cardSymbol = '&heartsuit;'   
    }
    return cardSymbol
}


const dealFirstHand = () => {
    disableButtons()
    let counter = 0;

    let insertInterval = setInterval(function() {
        if(counter === 4) {
            enableButtons()
            checkForAces('player')
            p1Score.textContent = gameState.playerScore;
            if(gameState.dealerHand[0].value === 1) {
                p2Score.textContent = 11;  
            } else {
                p2Score.textContent = gameState.dealerScore - gameState.dealerHand[1].value;
            }

            clearInterval(insertInterval)

        } else {
            // NEED TO FINISH ACE LOGIC FOR DEALER ====================
            let card = dealSingleCard(deckShuffled)
            let cardHTML = createSingleCard(card)
            cardHTML.classList.add('card-show')
            if(counter === 3) {
                const div = document.createElement('div')
                div.classList.add('card')
                const html = `
                    <img style="width: 200px; transform: translate(-40px, -20px);" src="./img/playing-cards-back.png" />
                `
                div.innerHTML = html
                gameState.dealerHand.push(card)
                gameState.dealerScore+= card.value
                player2ResultsContainer.appendChild(div) 
                console.log(gameState) 
             
            } else if(counter % 2 === 0) {
                // ADD TO STATE
                if(card.rank === 'A') {
                    gameState.playerAces = true;
                    gameState.playerAcesLength.push(card);
                }
                gameState.playerHand.push(card) 
                gameState.playerScore+= card.value
                // ADD TO UI
                player1ResultsContainer.appendChild(cardHTML)
                // p1Score.textContent = gameState.playerScore;
                p1ScoreContainer.style.display = 'block';

            } else {
                // NEED TO FINISH ACE LOGIC FOR DEALER ====================
                // ADD TO STATE
                gameState.dealerHand.push(card)
                gameState.dealerScore+= card.value
                // ADD TO UI 
                player2ResultsContainer.appendChild(cardHTML)
                p2ScoreContainer.style.display = 'block';
            }
                counter++
        }
    }, 1000)

}

const checkForAces = (playerType) => {
    if(playerType === 'player') {
        if(gameState.playerAces) {
            console.log('WE HAVE AN ACE');    
            if(gameState.playerScore === 11) {
                gameState.playerScore = 21;
                p1Score.textContent = gameState.playerScore;
            }
            if(gameState.playerScore === 10) {
                console.log('we have 10');  
                gameState.playerScore+= 11;
                p1Score.textContent = gameState.playerScore;
            }
            if(gameState.playerScore < 10) {
                console.log('we are less than 10');  
                gameState.playerScore+= 10;
                p1Score.textContent = gameState.playerScore;
            }
        } 
    } 

    if(playerType === 'dealer') {
        console.log('DEALER CHECK!!!!')      
    }

}
const dealSingleCard = (shuffledDeck) => {
    let singleCard = shuffledDeck.shift()
    return singleCard
}

const getScore = (player) => {
    return gameState[player]
}

const resetState = () => {
    gameState.playerHand = [],
    gameState.dealerHand = [],
    gameState.playerScore = 0,
    gameState.dealerScore = 0
    gameState.playerAces = false,
    gameState.playerAcesLength = []
    return
}

const resetUI = () => {
    p1Score.textContent = gameState.playerScore
    p2Score.textContent = gameState.dealerScore
    player1ResultsContainer.innerHTML = ''
    player2ResultsContainer.innerHTML = ''
    return
}

const enableButtons = () => {
    btnHit.style.opacity = 1
    btnHit.disabled = false
    btnHold.style.opacity = 1
    btnHold.disabled = false
    return    
}

const disableButtons = () => {
    btnHit.style.opacity = 0.2
    btnHit.disabled = true
    btnHold.style.opacity = 0.2
    btnHold.disabled = true
    return
}

const endGameMsg = (obj) => {
    setTimeout(() => {
        playAgainModal.classList.add('play-again-modal-show');
        finalScore.textContent = `${obj.winner} ${obj.msg}`
        finalScore.classList.add('final-score-show')
    }, 1000)
}

const showDealerHiddenCard = () => {
    checkForAces('dealer')
    const card = gameState.dealerHand[1]
    const cardHTML = createSingleCard(card)
    const hiddenCardDOM = player2ResultsContainer.firstElementChild.nextElementSibling;
    hiddenCardDOM.parentNode.removeChild(hiddenCardDOM)
    player2ResultsContainer.appendChild(cardHTML)
}

const getScores = () => {
    let dealerScore = getScore('dealerScore')
    let playerScore = getScore('playerScore')
    p2Score.textContent = dealerScore
    return {
        dealerScore,
        playerScore
    }
}

const checkGameScores = (dealerScore, userScore) => {
    if(gameState.dealerScore > 21) {   
        endGameMsg({msg:gameState.playerScore, winner: 'Dealer Over - You win with a score of'})
        gameState.playerWins++
    } else if(gameState.dealerHand.length === 5) {
        endGameMsg({msg:gameState.dealerScore, winner: '5 CARD CHARLIE!! Dealer wins with a score of'})
        gameState.dealerWins++
    } else if(dealerScore > userScore || dealerScore === 21 || dealerScore === userScore) {
        endGameMsg({msg:gameState.dealerScore, winner: 'Dealer wins with a score of'})
        gameState.dealerWins++
    } else {
        setTimeout(function () {
            let card = dealSingleCard(deckShuffled)
            let cardHTML = createSingleCard(card)
            gameState.dealerHand.push(card) 
            gameState.dealerScore+= card.value 
            player2ResultsContainer.appendChild(cardHTML)
            p2Score.textContent = gameState.dealerScore 
            checkGameScores(gameState.dealerScore, gameState.playerScore)       
        },1500)
    }
}

const shuffleAndDeal = () => {
    deckShuffled = getShuffledDeck(getDeck);
    setTimeout(() => {
        dealFirstHand(deckShuffled)
    },700);
}



let getDeck = createDeck(rank, suit);
let deckShuffled;

btnHit.addEventListener('click', ()=> {
    let card = dealSingleCard(deckShuffled)
    let cardHTML = createSingleCard(card)

    if(card.rank === 'A') {
        gameState.playerAces = true;
        checkForAces('player')
        gameState.playerHand.push(card);
        player1ResultsContainer.appendChild(cardHTML)
        p1Score.textContent = gameState.playerScore
    
    } else {
        gameState.playerHand.push(card) 
        gameState.playerScore+= card.value
        player1ResultsContainer.appendChild(cardHTML)
        p1Score.textContent = gameState.playerScore
    }

    const player = 'playerScore'
    const userScore = getScore(player)
    if(userScore > 21) {
        showDealerHiddenCard()
        p2Score.textContent = gameState.dealerScore
        disableButtons()
        endGameMsg({msg:gameState.dealerScore, winner: 'BUST!!! - Dealer wins with a score of'})
        gameState.dealerWins++
    } else if(gameState.playerHand.length === 5) {
        endGameMsg({msg:gameState.playerScore, winner: '5 CARD CHARLIE!! You win with a score of'})
        gameState.playerWins++
        disableButtons()
    } 
})

btnHold.addEventListener('click', ()=> {
    showDealerHiddenCard()
    disableButtons()
    const scores = getScores();
    checkGameScores(scores.dealerScore, scores.playerScore)   
})

startBtn.addEventListener('click', () => {
    sideBar.classList.add('sidebar-show');
    headerTitleSpanContainer.classList.add('header-title-span-container-hide');
    btnHit.classList.add('btn-show-btn');
    btnHold.classList.add('btn-show-btn');
    headerTitleTopCenter.classList.add('header__title-top-center-show');
    gameAreaBoard.classList.add('game-area-board-show');
    shuffleAndDeal();
})


